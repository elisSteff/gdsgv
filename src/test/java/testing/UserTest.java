package testing;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class UserTest {
    @Autowired
    private  user user ;

    @Test
    public void adult(){
        String result = user.checkId(19);
        assertEquals("adult", result);
    }

    @Test
    public void minor(){
        String result = user.checkId(7);
        assertEquals("minor", result);
    }
}
